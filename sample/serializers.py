from rest_framework import serializers
from .models import Bucketlist, User

class BucketListSerializer(serializers.ModelSerializer):
    '''to serialize from model to json'''
    class Meta:
        model = Bucketlist
        fields = ('id','name', 'date_created')
        # read_only_fields = ('date_created','date_modified')
        # fields = "__all__"

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('user_key','name','password','user_group','email',)
        # fields = ('pk','gender',)
        # fields = "__all__"


