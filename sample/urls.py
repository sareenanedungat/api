
from django.urls import path
from django.conf.urls import url
from sample import views
from rest_framework.urlpatterns import format_suffix_patterns




 

urlpatterns = [
    url(r'^api/', views.BucketlistView.as_view()),
    url(r'^users/$', views.UserView.as_view()),
    # path('user/<str:user_key>/', views.UserDetails.as_view()),
    url(r'^user/view/(?P<user_key>\d+)/$', views.UserDetails.as_view(),name="user_details"),
    url(r'^user/delete/(?P<user_key>\d+)/$', views.UserDelete.as_view(),name="user_delete"),
  
]
