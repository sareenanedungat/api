from django.shortcuts import render
import requests
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response 
from rest_framework import status   
from .models import Bucketlist, User
from .serializers import BucketListSerializer , UserSerializer
from rest_framework import viewsets
# from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser 

class BucketlistView(APIView):
    def get(self, request):
        bucket = Bucketlist.objects.all()
        serializer = BucketListSerializer(bucket, many=True)
        return Response(serializer.data)
    
    def post(self, request):
        pass

class UserView(APIView):
    def get(self, request):
        user = User.objects.all()
        serializer = UserSerializer(user,many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer = UserSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserDetails(APIView):
    def get(self,request,user_key):
        # print(user_key)
        try:
            user_detail = User.objects.get(pk= user_key)
            # print(user_detail)
        except User.DoesNotExist:
            raise Http404    
        serializer = UserSerializer(user_detail)
        serializer.Meta.fields='__all__'
        return Response(serializer.data)     

    def post(self, request, user_key):
        user_detail = User.objects.get(pk=user_key)
        serializer = UserSerializer(data=request.data, instance = user_detail)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

class UserDelete(APIView):
    
    # def get(self,request):
    #     pass

    def post(self, request, user_key):
        
        try:
            user_obj = User.objects.get(user_key=user_key)
        except User.DoesNotExist:
            raise Http404
        user_obj.delete() 
        return Response(status=status.HTTP_200_OK)  


        
     






# def user_list(request):
#     if request.method == 'GET':
#         user_obj = User.objects.all()

#         name = request.query_params.get('user_key', None)
#         print(name)
#         if name is not None:
#             user_obj = user_obj.filter('user_key')

#         serializer = UserSerializer(user_obj, many=True)    
#         return JsonResponse(serializer.data, safe=False)

#     elif request.method == 'POST':
#         user_data = JSONParser().parse(request)
#         serializer = UserSerializer(data=user_data )   

#         if seriaizer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=status.HTTP_201_CREATED) 
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

#     elif request.method == "DELETE":
#         count = User.objects.all().delete()
#         return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)    


# @api_view(['GET', 'PUT', 'DELETE'])
# def user_detail(request, user_key):
#     try: 
#         user = User.objects.get(user_key=user_key) 
#     except User.DoesNotExist: 
#         return JsonResponse({'message': 'The user does not exist'}, status=status.HTTP_404_NOT_FOUND) 
 
#     if request.method == 'GET': 
#         serializer = UserSerializer(user) 
#         return JsonResponse(serializer.data) 
 
#     elif request.method == 'PUT': 
#         user_data = JSONParser().parse(request) 
#         serializer = UserSerializer(user, data=user_data) 
#         if serializer.is_valid(): 
#             serializer.save() 
#             return JsonResponse(serializer.data) 
#         return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
#     elif request.method == 'DELETE': 
#         user.delete() 
#         return JsonResponse({'message': 'User was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


