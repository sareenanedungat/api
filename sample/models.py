from django.db import models

class Bucketlist(models.Model):
    name = models.CharField(max_length=255, blank=False, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class User(models.Model):
    name = models.CharField(max_length=100)
    user_key= models.AutoField(primary_key=True)
    user_group = models.CharField(max_length=100,blank=True, null=True)
    gender = models.CharField(max_length=50,blank=True, null=True)
    email = models.EmailField(max_length=100,blank=True, null=True)
    mobile = models.CharField(max_length=50,blank=True, null=True)
    country = models.CharField(max_length=100,blank=True, null=True)
    age = models.IntegerField(null=True,blank=True, )
    address = models.CharField(max_length=150,blank=True, null=True)
    password = models.CharField(max_length=150,blank=True, null=True)
    pincode = models.IntegerField(null=True)

