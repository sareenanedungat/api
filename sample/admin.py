from django.contrib import admin
from .models import Bucketlist,User

admin.site.register(Bucketlist)
admin.site.register(User)

